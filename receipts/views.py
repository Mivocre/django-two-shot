from django.shortcuts import render, redirect
from django.http import HttpRequest, HttpResponse
from receipts.models import ExpenseCategory, Account, Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CategoryForm, AccountForm


# Create your views here.
@login_required
def all_receipts(request: HttpRequest) -> HttpResponse:
    try:
        receipts = Receipt.objects.filter(purchaser=request.user)
    except Exception:
        receipts = Receipt.objects.all()
    context = {"receipts": receipts}
    return render(request, "receipts/receipts.html", context)


@login_required
def create_receipt(request):
    if request.method == "GET":
        form = ReceiptForm()
    elif request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    return render(request, "receipts/create.html", {"form": form})


@login_required
def category_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    allcat = ExpenseCategory.objects.all()
    catlist = {}
    for cat in receipts:
        if cat.category not in catlist:
            catlist[cat.category] = 1
        else:
            catlist[cat.category] += 1
    for cat in allcat:
        if cat not in catlist:
            catlist[cat] = 0
    print(catlist)
    context = {"catlist": catlist}
    return render(request, "receipts/categories.html", context)


@login_required
def account_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    allacc = Account.objects.all()
    acclist = {}
    for acc in receipts:
        if acc.account not in acclist:
            acclist[acc.account] = [acc.account.number, 1]
        else:
            acclist[acc.account][1] += 1
    for acc in allacc:
        if acc not in acclist:
            acclist[acc] = 0
    print(acclist)
    context = {"acclist": acclist}
    return render(request, "receipts/accounts.html", context)


@login_required
def create_category(request):
    if request.method == "GET":
        form = CategoryForm()
    elif request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("category_list")
    return render(request, "receipts/categorycreate.html", {"form": form})


@login_required
def create_account(request):
    if request.method == "GET":
        form = AccountForm()
    elif request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("account_list")
    return render(request, "receipts/accountcreate.html", {"form": form})
